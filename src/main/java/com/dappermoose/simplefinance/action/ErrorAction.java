package com.dappermoose.simplefinance.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

// TODO: Auto-generated Javadoc
/**
 * The Class ErrorAction.
 */
@Controller
public class ErrorAction
{

    /**
     * Error action.
     *
     * @param request - the servlet request
     * @param model the model
     * @return the string
     */
    @RequestMapping ("/error")
    public String errorAction (final HttpServletRequest request,
            final Model model)
    {
        model.addAttribute ("status",
                request.getAttribute ("javax.servlet.error.status_code"));
        model.addAttribute ("reason",
                request.getAttribute ("javax.servlet.error.message"));

        return "httpError";
    }
}
