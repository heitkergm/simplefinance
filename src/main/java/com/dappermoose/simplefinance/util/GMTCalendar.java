package com.dappermoose.simplefinance.util;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

// TODO: Auto-generated Javadoc
/**
 * The Class GMTCalendar.
 */
public final class GMTCalendar
{
    /**
     * Gets the GMT calendar.
     *
     * @return the GMT calendar
     */
    public static Calendar getGMTCalendar ()
    {
        return GregorianCalendar.from (ZonedDateTime.now (ZoneOffset.UTC));
    }
    
    private GMTCalendar ()
    {
    }
}
