package com.dappermoose.simplefinance.util;

import java.sql.Timestamp;
import java.time.Instant;

// TODO: Auto-generated Javadoc
/**
 * The Class TimestampNow.
 */
public final class TimestampNow
{
    /**
     * Gets the.
     *
     * @return the timestamp
     */
    public static Timestamp get ()
    {
        return Timestamp.from (Instant.now ());
    }
    
    private TimestampNow ()
    {
    }
}
