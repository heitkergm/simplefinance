package com.dappermoose.simplefinance.data;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.Type;

import com.dappermoose.simplefinance.util.TimestampNow;

/**
 * The BaseEntity class.
 */
@MappedSuperclass
public class BaseModifiableEntity extends BaseEntity
{
    // database will set a default value
    @Column (name = "MODIFIED_AT", nullable = false)
    @Type (type = "com.dappermoose.simplefinance.data.UtcTimestampType")
    private Timestamp modified;

    /**
     * Sets the timestamps.
     * <p/>
     * Ensure that the time stored is ALWAYS GMT
     *
     */
    @Override
    @PrePersist
    @PreUpdate
    public void setTimestamps ()
    {
        super.setTimestamps ();
        if (modified == null)
        {
            modified = getCreated ();
        }
        else
        {
            modified = TimestampNow.get ();
        }
    }

    /**
     * Gets the modified.
     *
     * @return the modified
     */
    public Timestamp getModified ()
    {
        return modified;
    }

    /**
     * Sets the modified.
     *
     * @param modifiedNew the new modified
     */
    public void setModified (final Timestamp modifiedNew)
    {
        modified = modifiedNew;
    }
}
