package com.dappermoose.simplefinance.data;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import org.hibernate.annotations.Type;

import com.dappermoose.simplefinance.util.TimestampNow;

/**
 * The BaseEntity class.
 */
@MappedSuperclass
public class BaseEntity
{
    /** The created. */
    @Column (name = "CREATED_AT", nullable = false, updatable = false)
    @Type (type = "com.dappermoose.simplefinance.data.UtcTimestampType")
    private Timestamp created;

    @Version
    @Column (name = "VERSION", nullable = false)
    private Long version;

    /**
     * Sets the timestamps.
     *<p/>
     * Ensure that the time stored is ALWAYS GMT
     *
     */
    @PrePersist
    @PreUpdate
    public void setTimestamps ()
    {
        if (created == null)
        {
            created = TimestampNow.get ();
        }
    }

    /**
     * Gets the created.
     *
     * @return the created
     */
    public Timestamp getCreated ()
    {
        return created;
    }

    /**
     * Sets the created.
     *
     * @param createdNew the new created
     */
    public void setCreated (final Timestamp createdNew)
    {
        created = createdNew;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public Long getVersion ()
    {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param versionNew the new version
     */
    public void setVersion (final Long versionNew)
    {
        version = versionNew;
    }
}
