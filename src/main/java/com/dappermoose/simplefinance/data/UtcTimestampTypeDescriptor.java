package com.dappermoose.simplefinance.data;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.hibernate.type.descriptor.ValueBinder;
import org.hibernate.type.descriptor.ValueExtractor;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.JavaTypeDescriptor;
import org.hibernate.type.descriptor.sql.BasicBinder;
import org.hibernate.type.descriptor.sql.BasicExtractor;
import org.hibernate.type.descriptor.sql.TimestampTypeDescriptor;

import com.dappermoose.simplefinance.util.GMTCalendar;

// TODO: Auto-generated Javadoc
/**
 * The Class UtcTimestampTypeDescriptor.
 */
public class UtcTimestampTypeDescriptor extends TimestampTypeDescriptor
{
    /**
     * serialVersionID.
     */
    private static final long serialVersionUID = 1L;

    /** The Constant INSTANCE. */
    @SuppressWarnings ("hiding")
    public static final UtcTimestampTypeDescriptor INSTANCE = new UtcTimestampTypeDescriptor ();

    /*
     * (non-Javadoc)
     *
     * @see
     * org.hibernate.type.descriptor.sql.TimestampTypeDescriptor#getBinder(org
     * .hibernate.type.descriptor.java.JavaTypeDescriptor)
     */
    @Override
    public <X> ValueBinder<X> getBinder (
            final JavaTypeDescriptor<X> javaTypeDescriptor)
    {
        return new BasicBinder<X> (javaTypeDescriptor, this)
        {
            @Override
            protected void doBind (final PreparedStatement st, final X value,
                    final int index, final WrapperOptions options)
                    throws SQLException
            {
                // GregorianCalendar.instance(TimeZone) returns
                // the current time in the specified timezone.
                // The
                // GregorianCalendar.from(ZonedDateTime.new(ZoneOffset.UTC)))
                // does the same thing, in a thread-safe way.
                st.setTimestamp (index, javaTypeDescriptor.unwrap (value,
                        Timestamp.class, options), GMTCalendar
                        .getGMTCalendar ());
            }
        };
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.hibernate.type.descriptor.sql.TimestampTypeDescriptor#getExtractor
     * (org.hibernate.type.descriptor.java.JavaTypeDescriptor)
     */
    @Override
    public <X> ValueExtractor<X> getExtractor (
            final JavaTypeDescriptor<X> javaTypeDescriptor)
    {
        return new BasicExtractor<X> (javaTypeDescriptor, this)
        {
            @Override
            protected X doExtract (final ResultSet rs, final String name,
                    final WrapperOptions options) throws SQLException
            {
                return javaTypeDescriptor.wrap (
                        rs.getTimestamp (name, GMTCalendar.getGMTCalendar ()),
                        options);
            }

            @Override
            protected X doExtract (final CallableStatement statement,
                    final int index, final WrapperOptions options)
                    throws SQLException
            {
                throw new SQLException ("UTC doExtract - 1 - not implemented");
            }

            @Override
            protected X doExtract (final CallableStatement statement,
                    final String name, final WrapperOptions options)
                    throws SQLException
            {
                throw new SQLException ("UTC doExtract - 2 - not implemented");
            }
        };
    }
}
