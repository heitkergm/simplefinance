package com.dappermoose.simplefinance.data;

// TODO: Auto-generated Javadoc
/**
 * The Enum YesNoEnum.
 */
public enum YesNoEnum
{
    /** The yes. */
    YES,
    /** The no. */
    NO;
}
