package com.dappermoose.simplefinance.data;

import java.util.Comparator;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.LiteralType;
import org.hibernate.type.TimestampType;
import org.hibernate.type.VersionType;
import org.hibernate.type.descriptor.java.JdbcTimestampTypeDescriptor;

// TODO: Auto-generated Javadoc
/**
 * The Class UtcTimestampType.
 */
public class UtcTimestampType extends
        AbstractSingleColumnStandardBasicType<Date> implements
        VersionType<Date>, LiteralType<Date>
{

    /**
     * This is the serial version ID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * This is the Constant INSTANCE.
     */
    public static final UtcTimestampType INSTANCE = new UtcTimestampType ();

    /**
     * Instantiates a new utc timestamp type.
     */
    public UtcTimestampType ()
    {
        super (UtcTimestampTypeDescriptor.INSTANCE,
                JdbcTimestampTypeDescriptor.INSTANCE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.hibernate.type.Type#getName()
     */
    @Override
    public String getName ()
    {
        return TimestampType.INSTANCE.getName ();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.hibernate.type.AbstractStandardBasicType#getRegistrationKeys()
     */
    @Override
    public String[] getRegistrationKeys ()
    {
        return TimestampType.INSTANCE.getRegistrationKeys ();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.hibernate.type.VersionType#next(java.lang.Object,
     * org.hibernate.engine.spi.SessionImplementor)
     */
    @Override
    public Date next (final Date current, final SessionImplementor session)
    {
        return TimestampType.INSTANCE.next (current, session);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.hibernate.type.VersionType#seed(org.hibernate.engine.spi.
     * SessionImplementor)
     */
    @Override
    public Date seed (final SessionImplementor session)
    {
        return TimestampType.INSTANCE.seed (session);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.hibernate.type.VersionType#getComparator()
     */
    @Override
    public Comparator<Date> getComparator ()
    {
        return TimestampType.INSTANCE.getComparator ();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.hibernate.type.LiteralType#objectToSQLString(java.lang.Object,
     * org.hibernate.dialect.Dialect)
     */
    @Override
    public String objectToSQLString (final Date value, final Dialect dialect)
            throws Exception
    {
        return TimestampType.INSTANCE.objectToSQLString (value, dialect);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.hibernate.type.AbstractStandardBasicType#fromStringValue(java.lang
     * .String)
     */
    @Override
    public Date fromStringValue (final String xml) throws HibernateException
    {
        return TimestampType.INSTANCE.fromStringValue (xml);
    }
}
