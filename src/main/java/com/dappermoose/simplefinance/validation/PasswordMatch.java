package com.dappermoose.simplefinance.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

// TODO: Auto-generated Javadoc
/**
 * The Interface PasswordMatch.
 */
@Retention (RetentionPolicy.RUNTIME)
@Target ({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Constraint (validatedBy = PasswordMatchImpl.class)
public @interface PasswordMatch
{
    /**
     * Message.
     *
     * @return the error message
     */
    String message() default "{notmatch.password}";

    /**
     * Groups.
     *
     * @return the array of classes in the group
     */
    Class<?>[] groups() default {};

    /**
     * Payload.
     *
     * @return the array of classes in the payload
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * First field.
     *
     * @return the first field
     */

    String password();

    /**
     * Second field.
     *
     * @return the second field
     */
    String repassword();
}
