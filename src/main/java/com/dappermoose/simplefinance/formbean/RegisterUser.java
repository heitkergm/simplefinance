package com.dappermoose.simplefinance.formbean;

import javax.validation.constraints.Size;

import com.dappermoose.simplefinance.validation.PasswordMatch;

// TODO: Auto-generated Javadoc
/**
 * The Class RegisterUser.
 */
@PasswordMatch (password = "password", repassword = "repeatedPassword", message = "{register.pwd.notmatch}")
public class RegisterUser extends LoginUser
{
    /** The repeated password. */
    @Size (min = 1, max = 32, message = "{register.secondPassword.size}")
    private String repeatedPassword;

    /**
     * Gets the repeated password.
     *
     * @return the repeated password
     */
    public String getRepeatedPassword ()
    {
        return repeatedPassword;
    }

    /**
     * Sets the repeated password.
     *
     * @param repeatedPasswordNew the new repeated password
     */
    public void setRepeatedPassword (final String repeatedPasswordNew)
    {
        repeatedPassword = repeatedPasswordNew;
    }
}
