package com.dappermoose.simplefinance.init;

import java.lang.reflect.Proxy;
import java.util.Properties;

import javax.inject.Inject;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.sql.DataSource;

import net.sf.ehcache.management.ManagementService;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.stat.Statistics;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.jmx.support.MBeanServerFactoryBean;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.ui.context.support.ResourceBundleThemeSource;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.RequestDataValueProcessor;
import org.springframework.web.servlet.theme.SessionThemeResolver;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.dappermoose.simplefinance.interceptor.AddResponseHeadersInterceptor;
import com.dappermoose.simplefinance.interceptor.CSRFInterceptor;
import com.dappermoose.simplefinance.interceptor.MustBeLoggedInInterceptor;
import com.dappermoose.simplefinance.interceptor.SessionInterceptor;
import com.dappermoose.simplefinance.jmx.JMXStatistics;
import com.dappermoose.simplefinance.util.HiddenFields;

// TODO: Auto-generated Javadoc
/**
 * The Class SpringConfig.
 */
@EnableWebMvc
@EnableTransactionManagement
@EnableMBeanExport
@EnableSpringDataWebSupport
@ComponentScan (basePackages = { "com.dappermoose.simplefinance.data",
        "com.dappermoose.simplefinance.action",
        "com.dappermoose.simplefinance.errors" })
@EnableJpaRepositories (basePackages = { "com.dappermoose.simplefinance.dao" })
@PropertySource ("classpath:jdbc-${db:postgres}.properties")
@Configuration
public class SpringConfig extends WebMvcConfigurerAdapter
{

    /** The env. */
    @Inject
    Environment env;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #configureDefaultServletHandling(org.springframework.web.servlet.config.
     * annotation.DefaultServletHandlerConfigurer)
     */
    @Override
    public void configureDefaultServletHandling (
            final DefaultServletHandlerConfigurer configurer)
    {
        configurer.enable ();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addResourceHandlers(org.springframework.web.servlet.config.annotation.
     * ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers (final ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler ("/js/**").addResourceLocations ("/js/");
        registry.addResourceHandler ("/css/**").addResourceLocations ("/css/");
        registry.addResourceHandler ("/images/**").addResourceLocations (
                "/images/");
        registry.addResourceHandler ("/webjars/**").addResourceLocations ("/webjars/");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addInterceptors(org.springframework.web.servlet.config.annotation.
     * InterceptorRegistry)
     */
    @Override
    public void addInterceptors (final InterceptorRegistry registry)
    {
        registry.addInterceptor (new AddResponseHeadersInterceptor ());
        registry.addInterceptor (new SessionInterceptor ());
        registry.addInterceptor (new MustBeLoggedInInterceptor ());
        registry.addInterceptor (csrfInterceptor ());
        final OpenEntityManagerInViewInterceptor openEntity = new OpenEntityManagerInViewInterceptor ();
        openEntity.setEntityManagerFactory (entityManagerFactory ()
                .getObject ());
        registry.addWebRequestInterceptor (openEntity);
        registry.addInterceptor (new LocaleChangeInterceptor ());
        registry.addInterceptor (new ThemeChangeInterceptor ());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addViewControllers(org.springframework.web.servlet.config.annotation.
     * ViewControllerRegistry)
     */
    @Override
    public void addViewControllers (final ViewControllerRegistry registry)
    {
        registry.addRedirectViewController ("/", "/main");
    }

    // bean for interceptor, so it gets message source injected.
    @Bean
    HandlerInterceptorAdapter csrfInterceptor ()
    {
        return new CSRFInterceptor ();
    }

    // beans for i18n
    /**
     * Message source.
     *
     * @return the message source
     */
    @Bean
    MessageSource messageSource ()
    {
        final ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource ();
        source.setCacheSeconds (60);
        source.setBasenames ("/WEB-INF/messages",
                "classpath:ValidationMessages");
        return source;
    }

    /**
     * Session locale resolver.
     *
     * @return the session locale resolver
     */
    @Bean
    SessionLocaleResolver localeResolver ()
    {
        final SessionLocaleResolver resolver = new SessionLocaleResolver ();
        return resolver;
    }

    // beans for themes
    /**
     * Resource bundle theme source.
     *
     * @return the resource bundle theme source
     */
    @Bean
    ResourceBundleThemeSource themeSource ()
    {
        final ResourceBundleThemeSource themeSource = new ResourceBundleThemeSource ();
        themeSource.setBasenamePrefix ("themes.");
        return themeSource;
    }

    /**
     * Session theme resolver.
     *
     * @return the session theme resolver
     */
    @Bean
    SessionThemeResolver themeResolver ()
    {
        final SessionThemeResolver resolver = new SessionThemeResolver ();
        resolver.setDefaultThemeName ("blue");
        return resolver;
    }

    // beans for views
    /**
     * Internal resource view resolver.
     *
     * @return the internal resource view resolver
     */
    @Bean
    public InternalResourceViewResolver viewResolver ()
    {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver ();
        resolver.setPrefix ("/WEB-INF/jsp/");
        resolver.setSuffix (".jsp");
        resolver.setViewClass (JstlView.class);
        return resolver;
    }

    // beans for forms
    /**
     * Request data value processor.
     *
     * @return the request data value processor
     */
    @Bean
    public RequestDataValueProcessor requestDataValueProcessor ()
    {
        return new HiddenFields ();
    }

    // beans for tx/database
    /**
     * Persistence post processor.
     *
     * @return the persistence exception translation post processor
     */
    @Bean
    public PersistenceExceptionTranslationPostProcessor persistencePostProcessor ()
    {
        return new PersistenceExceptionTranslationPostProcessor ();
    }

    /**
     * Data source.
     *
     * @return the data source
     */
    @Bean
    public DataSource dataSource ()
    {
        final BasicDataSource dataSource = new BasicDataSource ();
        dataSource.setDriverClassName (env.getProperty ("jdbc.driver"));
        dataSource.setUrl (env.getProperty ("jdbc.url"));
        dataSource.setUsername (env.getProperty ("jdbc.username"));
        dataSource.setPassword (env.getProperty ("jdbc.password"));
        dataSource.setMinIdle (1);
        return dataSource;
    }

    /**
     * Entity manager factory.
     *
     * @return the abstract entity manager factory bean
     */
    @Bean
    public AbstractEntityManagerFactoryBean entityManagerFactory ()
    {
        final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean ();
        bean.setJpaVendorAdapter (new HibernateJpaVendorAdapter ());
        bean.setDataSource (dataSource ());
        bean.setPersistenceUnitName ("simplefinancePersistence");
        bean.setPackagesToScan ("com.dappermoose.simplefinance.data");
        final Properties props = new Properties ();
        final String dialect = env.getProperty ("jdbc.dialect");
        if (dialect.indexOf ("Oracle") != -1)
        {
            props.put ("hibernate.jdbc.wrap_result_sets", "true");
        }
        props.put ("hibernate.dialect", dialect);
        props.put ("hibernate.connection.release_mode", "on_close");
        props.put ("hibernate.current_session_context_class", "managed");
        props.put ("hibernate.generate_statistics", "true");
        props.put ("hibernate.default_schema", env.getProperty ("jdbc.schema"));
        props.put ("hibernate.cache.use_query_cache", "true");
        props.put ("hibernate.cache.region.factory_class",
                "org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory");
        props.put ("hibernate.hbm2ddl.auto",
                env.getProperty ("jdbc.hbm2ddl.auto"));
        bean.setJpaProperties (props);
        return bean;
    }

    /**
     * Transaction manager.
     *
     * @return the abstract platform transaction manager
     */
    @Bean
    public AbstractPlatformTransactionManager transactionManager ()
    {
        return new JpaTransactionManager (entityManagerFactory ().getObject ());
    }

    // cache manager
    /**
     * Eh cache manager.
     *
     * @return the eh cache manager factory bean
     */
    @Bean
    public EhCacheManagerFactoryBean cacheManager ()
    {
        final EhCacheManagerFactoryBean bean = new EhCacheManagerFactoryBean ();
        bean.setShared (true);
        return bean;
    }

    // mbean beans
    // the @EnableMBeanExport exports ALL spring beans which are also mbeans
    // AND also all annotated mbeans
    /**
     * Mbean server.
     *
     * @return the m bean server factory bean
     */
    @Bean
    public MBeanServerFactoryBean mbeanServer ()
    {
        final MBeanServerFactoryBean bean = new MBeanServerFactoryBean ();
        bean.setLocateExistingServerIfPossible (true);
        return bean;
    }

    /**
     * Hib stats.
     *
     * @return the object
     * @throws MalformedObjectNameException the malformed object name exception
     * @throws MBeanRegistrationException the m bean registration exception
     * @throws NotCompliantMBeanException the not compliant m bean exception
     * @throws InstanceAlreadyExistsException the instance already exists
     *             exception
     */
    @Bean
    public Object hibStats () throws MalformedObjectNameException,
            MBeanRegistrationException, NotCompliantMBeanException,
            InstanceAlreadyExistsException
    {
        final ObjectName statsName = new ObjectName (
                "org.hibernate.jmx:type=Statistics");
        final SessionFactory sessionFactory = ((HibernateEntityManagerFactory) (entityManagerFactory ()
                .getObject ())).getSessionFactory ();
        final Statistics statistics = sessionFactory.getStatistics ();
        statistics.setStatisticsEnabled (true);
        final Object statisticsMBean = Proxy.newProxyInstance (getClass ()
                .getClassLoader (), new Class<?>[] { JMXStatistics.class }, (
                proxy, method, args) -> method.invoke (statistics, args));

        mbeanServer ().getObject ().registerMBean (statisticsMBean, statsName);
        return new Object ();
    }

    /**
     * Eh cache m bean.
     *
     * @return the object
     */
    @Bean
    public Object ehCacheMBean ()
    {
        ManagementService.registerMBeans (cacheManager ().getObject (),
                mbeanServer ().getObject (), true, true, true, true, true);
        return new Object ();
    }
}
