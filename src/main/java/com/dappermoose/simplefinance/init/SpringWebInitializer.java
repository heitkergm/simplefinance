package com.dappermoose.simplefinance.init;

import java.util.EnumSet;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

// TODO: Auto-generated Javadoc
/**
 * The Class SpringWebInitializer.
 */
public class SpringWebInitializer implements WebApplicationInitializer
{

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.web.WebApplicationInitializer#onStartup(javax.servlet
     * .ServletContext)
     */
    @Override
    public void onStartup (final ServletContext container)
    {
        // Create the loader listener Spring application context
        final AnnotationConfigWebApplicationContext loaderContext = new AnnotationConfigWebApplicationContext ();
        loaderContext.register (SpringConfig.class);

        // Create the dispatcher servlet's Spring application context
        final AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext ();
        dispatcherContext.register (SpringWebConfig.class);
        dispatcherContext.setParent (loaderContext);

        // Set the session tracking globally for this servlet context to Cookie.
        // This will override web.xml session tracking.
        final Set<SessionTrackingMode> modes = EnumSet
                .noneOf (SessionTrackingMode.class);
        modes.add (SessionTrackingMode.COOKIE);
        container.setSessionTrackingModes (modes);

        // Set the cookie config
        final SessionCookieConfig cookieConfig = container
                .getSessionCookieConfig ();
        cookieConfig.setHttpOnly (true);
        cookieConfig.setSecure (true);

        // register the context loader listener
        container.addListener (new ContextLoaderListener (loaderContext));

        // Register and map the dispatcher servlet
        final ServletRegistration.Dynamic dispatcher = container.addServlet (
                "dispatcher", new DispatcherServlet (dispatcherContext));
        dispatcher.setLoadOnStartup (1);
        dispatcher.addMapping ("/*");

        // get the jsp servlet
        final ServletRegistration jsp = container
                .getServletRegistration ("jsp");
        if (jsp != null)
        {
            jsp.addMapping ("/WEB-INF/jsp/*", "/WEB-INF/decorator/*");
        }
    }
}
