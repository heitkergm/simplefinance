package com.dappermoose.simplefinance.jmx;

import javax.management.MXBean;

import org.hibernate.stat.Statistics;

/**
 * The Interface JMXStatistics.
 */
@MXBean
public interface JMXStatistics extends Statistics
{
}
