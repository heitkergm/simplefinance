package com.dappermoose.simplefinance.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.simplefinance.data.Account;
import com.dappermoose.simplefinance.data.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface AccountRepository.
 */
public interface AccountRepository extends CrudRepository<Account, Long>
{

    /**
     * Find by user.
     *
     * @param user the user
     * @return the list
     */
    List<Account> findByUser (User user);
}
