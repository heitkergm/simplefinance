package com.dappermoose.simplefinance.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.simplefinance.data.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserRepository.
 */
public interface UserRepository extends CrudRepository<User, Long>
{

    /**
     * Find by user name.
     *
     * @param userName the user name
     * @return the list
     */
    List<User> findByUserName (String userName);
}
