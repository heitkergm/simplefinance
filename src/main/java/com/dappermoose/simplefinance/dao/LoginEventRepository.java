package com.dappermoose.simplefinance.dao;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.simplefinance.data.LoginEvent;

/**
 * The Interface LoginEventRepository.
 */
public interface LoginEventRepository extends CrudRepository<LoginEvent, Long>
{
}
