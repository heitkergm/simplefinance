
<!DOCTYPE html>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head><meta charset="UTF-8" /><title><spring:message code="error.title" /></title></head>
<body>
<div class="center">
    <h1><spring:message code="error.header" /></h1>
    <p><spring:message code="error.text" /></p>

    <!--
    Failed URL: ${url}
    Exception:  ${exception.message}
        <c:forEach items="${exception.stackTrace}" var="ste">
        ${ste} 
    </c:forEach>
    -->
    </div>
</body>
</html>