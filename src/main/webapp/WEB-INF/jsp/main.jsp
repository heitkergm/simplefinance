<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="main.title" /></title>
</head>
<body>

	<div class="center">
	    <table class="pure-table pure-table-striped">
	        <thead>
	            <tr>
	                <th>Account Name</th>
	                <th>Balance</th>
	            </tr>
	        </thead>
	        <tbody>
                <c:forEach items="${accounts}" var="account">
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td><spring:message code="main.total" /></td>
                    <td><fmt:formatNumber type="currency">${balance}</fmt:formatNumber></td>
                </tr>
            </tfoot>
        </table>
        <br /><a href="${pageContext.request.contextPath}/acct/add" class="pure-button button-small"><spring:message
					code="main.addAcctLink" /></a>
        
	</div>
</body>
</html>