    function collectFormData(fields) {
        var data = "{";
        for (var i = 0; i < fields.length; i++) {
            var $item = $(fields[i]);
            if(i > 0){
                data = data + ",";
                }
            data = data + "\"" + $item.attr('name') + "\"" + ": \"" + $item.val() + "\"";
        }
        data = data + "}";
        return data;
    }
	$(document).ready(function() {
		var $form = $(ajaxFormName);
		$form.bind('submit',
			function(e) {
				// Ajax validation
				var $inputs = $form.find('input');
				var inputData = collectFormData($inputs);
				var successValue = true;
				$.ajax({ type: "POST",
						url: ajaxUrl,
					    contentType: "application/json; charset=utf-8",
					    dataType: "json",
                        data: inputData,
                        async: false,
						success: function(response) {
							$form.find('.error').empty();
							if (response.status == 'FAIL') {
								var html = "";
                                for(var i = 0; i < response.errorMessageList.length; i++)
                                    html = html + response.errorMessageList[i].defaultMessage + "<br />";
							    $form.find('.error').html(html);
							    successValue = false;
							}
						}});
							return successValue;
						});
	});