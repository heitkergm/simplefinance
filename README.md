# SimpleFinance #

Welcome to SimpleFinance, by Matt.

This app draws on my 28+ years in IT, as well as 15+ years in Java and Web development.

Contact me at the email below.

Lots of ideas are in here, and not all of them are mine.  I've scoured the web for solutions to the many issues I've encountered, and have adapted the best ones to this project.

### Repository Contents ###

* This is a sample web app to demonstrate the usage of Spring, Hibernate JPA and other web  technologies.
* Version 1.0.0 - a rolling version until it's done.
* The project, classpath, and settings (project based, NOT workspace based) files for Eclipse Luna JavaEE are included in the repository.


### Technologies ###
* Java 1.8
* JSP 2.3
* Servlet 3.1
* JSTL 1.2
* JPA 2.1
* Spring 4.1
* Hibernate 4.3 (JPA entity Manager)
* Hibernate Validations 5.1
* SLF4J/Logback as the logging library.
* Sitemesh 3 (servlet filter perform view decoration)
* Postgres 9.3
* PureCSS
* FontAwesome
* Form validation via AJAX (JQuery)

### Goals ###
* Maven 3.2 is used as the project build engine.
* Tomcat 8.0 is target.
* minimal usage of xml configuration.
* Annotation and Java code preferred for configuration.
* working JMX beans for Hibernate and EhCache statistics, making these visible in JConsole.
* Working UTC timestamps in the database.  JDBC makes this **HARD** to do.
* Normalized database design.
* CSRF prevention (via nonce).
* Session hijacking prevention (successful login creates NEW session).
* Hibernate generates the database.
* Web app is configured for 100% HTTPS usage (see web.xml).

### Set Up ###

* See the [wiki](https://bitbucket.org/heitkergm/simplefinance/wiki)

### Contact Info ###

* Matt (heitkergm@acm.org)